<?php

/**
 * Created by PhpStorm.
 * User: Rim
 * Date: 21-May-15
 * Time: 11:43 AM
 */
session_start();
include "../models/user/models_user.php";
if (isset($_SESSION['user'])) {
    $username = $_SESSION['user'];
    $user_model = new User();
    include "../views/index/members.php";
} else {
    include "../views/user/index.php";
}
?>