
$(document).ready(function () {
    $(".menu li").hover(function () {
        $(this).find('.drop-content').css({visibility: "visible", display: "none"}).show(400);
    }, function () {
        $(this).find('.drop-content').css({visibility: "hidden"});
    });
});
$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#top').fadeIn();
        }
        else {
            $('#top').fadeOut();
        }
    });
    $('#top').click(function () {
        $('body,html').animate({scrollTop: 0}, 400);
    });
});