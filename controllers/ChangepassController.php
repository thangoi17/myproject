<?php 
	include "../models/user/models_user.php";
	session_start();
	class ChangepassController{
		public $model;
		public function __construct(){
			if(!$_SESSION['user']){
				header("location: ../controllers/LoginController.php");
			}else{$this->model= new User();}
			
		}
		public function getAll(){			
			$user=$this->model->get_user($_SESSION['user']);
			$row=mysql_fetch_array($user);
			return $row;
		}
		public function change_pass(){
				if(isset($_POST['submit'])){
					if($_POST['password']!=$_POST['re-password']){
						echo "Mật khẩu xác nhận không đúng";
					}else {
						$this->model->update_pass($_SESSION['user'],$_POST['password']);
						echo "<p style='color:red;'> Mật khẩu của bạn đã được đổi thành công</p>";					
					}
				}
		}
	}
	$change= new ChangepassController();
	include "../views/user/changepass.php";
 ?>