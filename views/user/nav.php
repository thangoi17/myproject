
       <div class="navigation">
              <div class="nav-slide-1">
                     <div class="slide">
                            <div class="content-slide">
                                   <div class="slide-stage">
                                          <div class="slide-image"><img class="img-slide" src="../assets/images/slider.png"/></div>
                                   </div>
                            </div>
                     </div>
              </div>
              <div class="nav-slide-2">
                     <div class="content-nav">
                            <p class="description">Sed porttitor ipsum at justo suscipit quis sollicitudin dolor facilisis. Fusce vestibumlum blandit ultricies.</p>
                            <a href="#" class="link-view">View Portvolio</a>
                            <ul class="news">
                                   <li class="item-news">
                                          <div class="item-news-top">
                                                 <a href="#" class="link-news" id="link-news">

                                                 </a>
                                                 <a class="title-news" href="#" id="title-news"></a>
                                                 <p class="content-news" id="content-news"></p>
                                          </div>
                                   </li>
                                   <li class="item-news item-center">
                                          <div class="item-news-top">
                                                 <a href="#" class="link-news" id="link-news-center">

                                                 </a>
                                                 <a class="title-news" href="#" id="title-news-center"></a>
                                                 <p class="content-news" id="content-news-center"></p>
                                          </div>
                                   </li>
                                   <li class="item-news item-right">
                                          <div class="item-news-top">
                                                 <a href="#" class="link-news" id="link-news-right">

                                                 </a>
                                                 <a class="title-news" href="#" id="title-news-right"></a>
                                                 <p class="content-news" id="content-news-right"></p>
                                          </div>
                                   </li>
                            </ul>
                     </div>
              </div>
       </div>
