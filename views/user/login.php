<!DOCTYPE html>
<html>
<head lang="en">
       <meta charset="UTF-8">
       <title>Log in</title>
       <link rel="stylesheet" type="text/css" href="../assets/css/login.css">
</head>
<body>
<div class="content-login">
       <div id="title">
              <h1>Welcome to Login page.</h1>
              <?php 
                     $a = new LoginControllers();
                     $a->login();
              ?>
       </div>
       <form action="#" method="post">
              <div class="content-form-login">
                     <div class="input-form">
                            <span>Username</span>
                            <input type="text" name="username" required="">
                     </div>
                     <div class="input-form">
                            <span>Password</span>
                            <input type="password" name="password" required="">
                     </div>
                     <div class="input-form">
                            <p style="float: left;">Chưa có tài khoản? <a href="../controllers/SignupController.php" id ="signup-now">Đăng ký ngay!</a></p>
                            <input type="submit" name="submit" value="Login" required="">
                     </div>
              </div>
       </form>
</div>
</body>
</html>
