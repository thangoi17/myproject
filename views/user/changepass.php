<!DOCTYPE html>
<html>
<head lang="en">
       <meta charset="UTF-8">
       <title>Thay đổi mất khẩu</title>
       <link rel="stylesheet" type="text/css" href="../assets/css/style.css">       
</head>
<body style="text-align:center;">
	<?php include "../views/user/header.php"; ?>
	<div style="height:500px;">
		
		<div class="menu-left">
            <ul>
                <li>                                   
                    <a href="../trangchu">Trang chủ</a>
                </li>
                <li>                                   
                    <a href="../controllers/ChangePass.php">Đổi mật khẩu</a>
                </li>   
                <li >                                  
                    <a href="../controllers/LogoutController.php">Đăng xuất</a>
                </li>  
                <li style="height:280px;">
                </li>          
            </ul>

        </div>
        <div class='center' style="width:100%;">
        		<p style="color: #0074a2;color: #0074a2;font-size: 33px; font-weight: bold;padding-top: 20px;font-family: sans-serif;">Trang chỉnh sửa thông tin.</p>
                <?php $change->change_pass(); ?>
      			<div class="changepass">
                    <form action="#" method="post" class="form-changepass">
                        <p>Username</p>
                        <input type="text" name="username" value="<?php echo $_SESSION['user']; ?>" disabled>
                        <p>Password</p>
                        <input type="password" name="password" value="<?php echo $change->getAll()['password']; ?>">
                        <p>Re-password</p>
                        <input type="password" name="re-password" value="<?php echo $change->getAll()['password']; ?>">
                        <p>Email</p>
                        <input type="email" name="email" value="<?php echo $change->getAll()['email']; ?>" disabled> <br>
                        <input type="submit" name="submit" value="Cập nhật">
                    </form>
                </div>
        </div>
    </div>
    <?php include "../views/user/footer.php"; ?>
</body>
</html>