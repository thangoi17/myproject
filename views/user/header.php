<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
       <title>Website</title>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
       <link rel="stylesheet" type="text/css" href="../assets/css/reset.css">
       <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
       <script type="text/javascript" src="../assets/js/myquery.js"></script>
       <script type="text/javascript" src="../assets/js/json.js"></script>
</head>
<body>
<div class="container">
       <!-- Phan header  -->
       <div class="header">
              <div class="content-header">
                     <p class="text-header">Continuum</p>
                     <div class="menu">
                            <ul class="content-menu">
                                   <li class="item-menu">
                                          <a href="#" class="link-menu home-menu" >Home</a>
                                   </li>
                                   <li class="item-menu">
                                          <a href="#" class="link-menu">About</a>
                                   </li>
                                   <li class="item-menu">
                                          <a href="#" class="link-menu">Contact</a>
                                   </li>
                                   <li class="item-menu">
                                          <a href="#" class="link-menu">Full Width</a>
                                   </li>
                                   <li class="item-menu">
                                          <a href="#" class="link-menu">Shortcodes</a>
                                   </li>
                                   <li class="item-menu">
                                          <?php 
                                                 if(isset($_SESSION['user'])){
                                                    echo "<a href='../trangchu' class='link-menu'>".$_SESSION['user']."</a>";    
                                                 }else{
                                                        echo"<a href='../controllers/LoginController.php' class='link-menu'>LogIn</a>
                                                               <form name='form-login' method='post' action='../controllers/LoginController.php' class='form-login'>
                                                                      <div class='drop-content'>
                                                                             <div class='nav-drop'>
                                                                                    <div class='username'>
                                                                                           <label>Username</label>
                                                                                           <input name='username' value='' type='text' required>
                                                                                    </div>
                                                                                    <div class='username'>
                                                                                           <label>Password</label>
                                                                                           <input name='password' value='' type='password' required>
                                                                                    </div>
                                                                                    <div class='check-pass'>
                                                                                           <input type='checkbox' name='remember-pass'>Remember Me
                                                                                           <a href='#' class='lost-pass'>Lost password?</a>
                                                                                    </div>
                                                                                    <div class='login'>
                                                                                           <input name='submit' type='submit' value='Log In'>
                                                                                           <a href='../controllers/signup.php' id ='signup'>Signup</a>
                                                                                    </div>
                                                                             </div>
                                                                      </div>
                                                               </form>    
                                                        ";
                                                 }
                                           ?>
                                          
                                          
                                   </li>
                            </ul>
                     </div>

              </div>
       </div>
       <!-- Phan header  -->