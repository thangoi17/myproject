<!DOCTYPE html>
<html>
<head lang="en">
       <meta charset="UTF-8">
       <title>Sign up</title>
       <link rel="stylesheet" type="text/css" href="../assets/css/signup.css">
</head>
<body>
<div class="content-login">
       <div id="title">
              <h1>Welcome to Signup page.</h1> 
              <?php $a= new SignupControllers();
              $a->signup(); ?>
       </div>
       <form action="#" method="post">
              <div class="content-form-login">
                      <div class="input-form">
                            <span>Username</span>
                            <input type="text" name="username" required="">
                     </div>
                     <div class="input-form">
                            <span>Password</span>
                            <input type="password" name="password" required="">
                     </div>
                    <div class="input-form">
                            <span>Re-Password</span>
                            <input type="password" name="re-password" required="">
                     </div>
                      <div class="input-form">
                            <span>Email</span>
                            <input type="email" name="email" required="">
                     </div>
                      <div class="input-form">
                            <input type="submit" name="submit" value="Sign Up"  required="">
                     </div>
              </div>
       </form>
</div>
</body>
</html>
